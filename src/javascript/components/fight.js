import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    let myFirstFighter = Object.assign(firstFighter,
      { isBlocked: false,
        currentHealth: firstFighter.health,
        healthIndicator:document.getElementById("left-fighter-indicator"),
        isAllowedCriticalHit: true
      });
    let mySecondFighter = Object.assign(secondFighter,
      { isBlocked: false,
        currentHealth: secondFighter.health,
        healthIndicator:document.getElementById("right-fighter-indicator"),
        isAllowedCriticalHit: true
      });

    document.addEventListener('keyup', function(e) {
      const keyCode = e.code;
      switch (keyCode) {
        case controls.PlayerOneBlock:
        {
          myFirstFighter.isBlocked = false;
          break;
        }
        case controls.PlayerTwoBlock:
        {
          mySecondFighter.isBlocked = false;
          break;
        }
      }
    });

    function checkWinner(defender, attacker){
      if(defender.currentHealth<0){
        defender.healthIndicator.style.width = "0%";
        resolve(attacker);
      }else {
        return false;
      }
    }

    function makeAttack(attacker, defender){
      if(!attacker.isBlocked){
        const damage = getDamage(attacker, defender);
        changeDefenderHealth(defender, attacker, damage);
      }
    }

    function changeDefenderHealth(defender, attacker, damage){
      defender.currentHealth -= damage;
      if(!checkWinner(defender, attacker)){
        defender.healthIndicator.style.width = `${100 * defender.currentHealth / mySecondFighter.health}%`;
      }
    }

    function makeCriticalAttack(attacker, defender){
      if(attacker.isAllowedCriticalHit) {
        attacker.isAllowedCriticalHit = false;
        const damage = getCriticalDamage(attacker);
        changeDefenderHealth(defender, attacker, damage);
        setTimeout(() => attacker.isAllowedCriticalHit = true, 10000);
      }
    }

    document.addEventListener('keydown', function(e) {
      const keyCode = e.code;
      switch (keyCode){
        case controls.PlayerOneAttack:
        {
          makeAttack(myFirstFighter, mySecondFighter);
          break;
        }
        case controls.PlayerOneBlock:
        {
          myFirstFighter.isBlocked = true;
          break;
        }
        case controls.PlayerTwoAttack:
        {
          makeAttack(mySecondFighter, myFirstFighter);
          break;
        }
        case controls.PlayerTwoBlock:
        {
          mySecondFighter.isBlocked = true;
          break;
        }
        case controls.PlayerOneCriticalHitCombination[1]&&controls.PlayerOneCriticalHitCombination[0]&&controls.PlayerOneCriticalHitCombination[2]:
        {
          makeCriticalAttack(myFirstFighter, mySecondFighter);
          break;
        }
        case controls.PlayerTwoCriticalHitCombination[1]&&controls.PlayerTwoCriticalHitCombination[0]&&controls.PlayerTwoCriticalHitCombination[2]:
        {
          makeCriticalAttack(mySecondFighter, myFirstFighter);
          break;
        }
      }
    })
  });
}

function getCriticalDamage(attacker) {
  return 2 * attacker.attack;
}

export function getDamage(attacker, defender) {
  if(defender.isBlocked)
    return Math.max(getHitPower(attacker) - getBlockPower(defender), 0);
  else
    return getHitPower(attacker);
}

export function getHitPower(fighter) {
  const min = 1, max = 2,
    criticalHitChance = Math.random() * (max - min) + min;
  return fighter.attack * criticalHitChance;
}

export function getBlockPower(fighter) {
  const min = 1, max = 2,
    dodgeChance = Math.random() * (max - min) + min;
  return fighter.defense * dodgeChance;
}
  
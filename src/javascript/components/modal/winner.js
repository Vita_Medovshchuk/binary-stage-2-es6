import { showModal } from './modal';
import { createFighterImage } from '../fighterPreview';

export function showWinnerModal(fighter) {
    const imgElement = createFighterImage(fighter)
    showModal({title: `The winner is ${fighter.name.toUpperCase()}!`,
        bodyElement: imgElement, onClose: () => location.reload() })
}
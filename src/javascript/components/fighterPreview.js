import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  if(fighter){
    const imgElement = createFighterImage(fighter);
    imgElement.style.height = "300px"
    fighterElement.append(imgElement);
    const describeElement = createElement({
      tagName: 'div',
      className: `fighter-preview-text`,
    });
    describeElement.innerHTML = `
      ${fighter.name}
      <br>health: ${fighter.health}
      <br>attack: ${fighter.attack}
      <br>defense: ${fighter.defense}`
    fighterElement.append(describeElement);
  }
  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
